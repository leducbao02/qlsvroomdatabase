package com.example.quanlysinhvienroomdatabase.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quanlysinhvienroomdatabase.R;
import com.example.quanlysinhvienroomdatabase.models.UserModel;
import java.util.List;

public class UserAdapter extends  RecyclerView.Adapter<UserAdapter.UserViewHolder>{
    private List<UserModel> mListUser;
    private IclickItemUser iclickItemUser;
   public interface IclickItemUser{
       void updateUser(UserModel userModel);
   }

    public UserAdapter(IclickItemUser iclickItemUser) {
        this.iclickItemUser = iclickItemUser;
    }

    public void setData(List<UserModel> mListUser) {
        this.mListUser = mListUser;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user,parent,false);
        return new UserViewHolder(view );
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        UserModel user = mListUser.get(position);

        if(user == null){
//            Log.d("aaaa","null");
            return;
        }

        holder.tvName.setText(user.getName());
        holder.tvAge.setText(user.getAge());
        holder.btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iclickItemUser.updateUser(user);
            }
        });


    }

    @Override
    public int getItemCount() {
        if(mListUser != null){
            return mListUser.size();
        }
       return 0;
    }

    public class  UserViewHolder extends RecyclerView.ViewHolder{
      private TextView tvName, tvAge;
      private Button btnUpdate;

      public UserViewHolder(@NonNull View itemView) {
          super(itemView);
          tvName = itemView.findViewById(R.id.tv_name);
          tvAge = itemView.findViewById(R.id.tv_age);
          btnUpdate = itemView.findViewById(R.id.btnUpdate);

      }
  }
}

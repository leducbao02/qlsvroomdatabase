package com.example.quanlysinhvienroomdatabase.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.quanlysinhvienroomdatabase.models.UserModel;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface UserDAO {


    @Insert
    void insertUser(UserModel userModel);

    @Query("SELECT * FROM student")
    List<UserModel> getListUser();

    @Query("SELECT * FROM student where name = :userName ")
    List<UserModel> checkUser(String  userName);

    @Update
    void updateUser(UserModel userModel);
}

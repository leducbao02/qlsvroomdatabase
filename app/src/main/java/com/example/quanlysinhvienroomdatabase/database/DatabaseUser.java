package com.example.quanlysinhvienroomdatabase.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.quanlysinhvienroomdatabase.models.UserModel;

@Database(entities = {UserModel.class},version = 2)
public abstract class DatabaseUser extends RoomDatabase {
    private static final String DATABASE_NAME = "user.db";
    private  static  DatabaseUser instance;

    public static synchronized DatabaseUser getInstance(Context context){
        if(instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(),DatabaseUser.class,DATABASE_NAME)
                    .allowMainThreadQueries()
                    .build();
        }

        return instance;
    }

    public  abstract UserDAO userDAO();
}

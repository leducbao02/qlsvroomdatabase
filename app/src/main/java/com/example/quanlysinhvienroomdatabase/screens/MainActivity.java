package com.example.quanlysinhvienroomdatabase.screens;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.quanlysinhvienroomdatabase.R;
import com.example.quanlysinhvienroomdatabase.adapter.UserAdapter;
import com.example.quanlysinhvienroomdatabase.database.DatabaseUser;
import com.example.quanlysinhvienroomdatabase.models.UserModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final int MY_REQUEST_CODE = 10;
    private EditText edt_name, edt_age;
    private Button btnAdd;
    private RecyclerView rvStudent;

    private UserAdapter userAdapter;
    private List<UserModel> mListUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView(){
        edt_name = findViewById(R.id.edtName);
        edt_age = findViewById(R.id.edt_age);
        btnAdd = findViewById(R.id.btn_add);
        rvStudent = findViewById(R.id.rcv_student);
        userAdapter = new UserAdapter(new UserAdapter.IclickItemUser() {
            @Override
            public void updateUser(UserModel userModel) {
                clickUpdateUser(userModel);
            }
        });
        userAdapter.setData(mListUser);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvStudent.setLayoutManager(layoutManager);
        rvStudent.setAdapter(userAdapter);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addUser();
            }
        });
     loadData();

    }

    private void clickUpdateUser(UserModel userModel) {
        Intent intent = new Intent(MainActivity.this,UpdateActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("object_user",userModel);
        intent.putExtras(bundle);
        startActivityForResult(intent,MY_REQUEST_CODE);
    }

    private void addUser() {
        String strUserName = edt_name.getText().toString().trim();
        String strUserAge = edt_age.getText().toString().trim();
        if (strUserName.isEmpty() || strUserAge.isEmpty()){
            Toast.makeText(this, "Vui lòng điền đầy đủ thông tin", Toast.LENGTH_SHORT).show();
           return;
        }
        UserModel userModel = new UserModel(strUserName,strUserAge);


        if (isUserExist(userModel)){
            Toast.makeText(this, "User này đã tồn tại", Toast.LENGTH_SHORT).show();
            return;
        }

        DatabaseUser.getInstance(this).userDAO().insertUser(userModel);
        Toast.makeText(this, "Add Student Successfully", Toast.LENGTH_SHORT).show();

        edt_name.setText("");
        edt_age.setText("");

        hideSoftKeyborad();
        loadData();

    }


    private void loadData(){
        mListUser = DatabaseUser.getInstance(this).userDAO().getListUser();
        userAdapter.setData(mListUser);
    }


    public boolean isUserExist(UserModel userModel){
        List<UserModel> listUser = DatabaseUser.getInstance(this).userDAO().checkUser(userModel.getName());
        return listUser != null && !listUser.isEmpty();
    }

    public void hideSoftKeyborad(){
        try{
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == MY_REQUEST_CODE && requestCode == Activity.RESULT_OK){
            loadData();
        }
    }
}
package com.example.quanlysinhvienroomdatabase.screens;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.quanlysinhvienroomdatabase.R;
import com.example.quanlysinhvienroomdatabase.database.DatabaseUser;
import com.example.quanlysinhvienroomdatabase.models.UserModel;

public class UpdateActivity extends AppCompatActivity {
    private EditText edt_name, edt_age;
    private Button btnUpdate;
    private UserModel userModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        edt_name = findViewById(R.id.edtName);
        edt_age = findViewById(R.id.edt_age);
        btnUpdate = findViewById(R.id.btn_update);

        userModel = (UserModel) getIntent().getExtras().get("object_user");
        if(userModel != null){
            edt_name.setText(userModel.getName());
            edt_age.setText(userModel.getAge());
        }

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUser();
            }
        });
    }

    private void updateUser() {
        String strUserName = edt_name.getText().toString().trim();
        String strUserAge = edt_age.getText().toString().trim();
        if (strUserName.isEmpty() || strUserAge.isEmpty()){
            Toast.makeText(this, "Vui lòng điền đầy đủ thông tin", Toast.LENGTH_SHORT).show();
            return;
        }

        //update user
        userModel.setName(strUserName);
        userModel.setAge(strUserAge);

        DatabaseUser.getInstance(this).userDAO().updateUser(userModel);
        Toast.makeText(this, "Update student successfully", Toast.LENGTH_SHORT).show();
        Intent intentResult = new Intent();
        setResult(Activity.RESULT_OK, intentResult);

        //destroy activity
        finish();


    }
}
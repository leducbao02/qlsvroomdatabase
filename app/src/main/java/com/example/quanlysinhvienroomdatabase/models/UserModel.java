package com.example.quanlysinhvienroomdatabase.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "student")
public class UserModel implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private String name;
    private String age;


    public UserModel( String name, String age) {
        this.name = name;
        this.age = age;
    }

    public UserModel() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }



}
